# Why we use a deploy system
Because we have our own hosting stack, we have a lot of control over the wayour code is handled. 
Most of our projects consist of a wordpress theme, so 1 method fits a lot.

## Why git?
- It's fast
- It forces you to use source control
- Every project already has a repository


# How our code moves from localhost to production
We use 2 repositories for the deploy system. 
The repositories used by our remote server are seperate from the origin repository we use to store our code. 

## Development version
- Development happens on the development branch on your local machine
- Changes in the development branch get pushed from your local machine to the remote development repository
- The remote development repository updates the staging environment

## Production version
- Merge the development branch into master
- The updated master branch is pushed to the remote production repository
- The remote production repository updates the production environment


# How this effects our workflow
All development happens on the development branch in your projects repository.

- All commits -> origin
- Nice update package -> staging
- Client has seen update -> merge master -> production

## How this affects you
2 Seperate deploy users on remote server
Public/Private key authentication


# What happens on deploy
- Repo gets updated & triggers script
- New checkout 
- Scripts and styles are compiled with grunt when neccesary
- Useless files are removed
- Archive is made
- Archive is copied to server
- Old code is replaced with code from archive

- Everybody smiles and eats cookies and such

 
